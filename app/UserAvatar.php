<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;

class UserAvatar extends Model
{
    public $timestamps = FALSE;

    protected $fillable = [
        'image'
    ];

    public static function createAvatar($name)
    {
        $profile_img_text = '';

        foreach (explode(' ', $name) as $key => $item) {
            if($key < 2)
                $profile_img_text .= strtoupper(substr($item,0,1));
        }

        $image_name = time().'.jpg';

        Image::canvas(400, 400, '#2dce89')
            ->text($profile_img_text, 200, 200, function($font) {
                $font->file(public_path('argon/fonts/roboto/Roboto-Bold.ttf'));
                $font->size(140);
                $font->color('#ffffff');
                $font->align('center');
                $font->valign('center');
            })
            ->save(public_path('images/users/' . $image_name));

        return $image_name;
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
