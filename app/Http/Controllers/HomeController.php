<?php

namespace App\Http\Controllers;

use App\Post;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $endpoint = date('Y-m-d');
        $access_key = env('EXCHANGERATESAPI_TOKEN');

        $base = 'EUR';
        $symbols = 'RON';

        $ch = curl_init('http://api.exchangeratesapi.io/v1/'.$endpoint.'?access_key='.$access_key.'&base='.$base.'&symbols='.$symbols);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result  = json_decode(curl_exec($ch), true);

        curl_close($ch);

        return view('dashboard', ['exchange_rate' => $result['rates']['RON']]);
    }

}
