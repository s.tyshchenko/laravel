<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\PostRequest;
use App\Post;
use App\PostCategory;
use App\PostImage;
use App\PostTag;
use App\Tag;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;
use Illuminate\View\View;

class PostController extends Controller
{
    /**
     * Display a listing of the posts.
     *
     * @return View
     */
    public function index(Post $model)
    {
        return view('posts.index', ['posts' => $model->paginate(15)]);
    }

    /**
     * Display posts by the specified search query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return View
     */
    public function search(Request $request)
    {
        $posts = Post::where('status', Post::PUBLISHED)
            ->where(function($q) use ($request) {
                    return $q->where('title', 'LIKE', '%' . $request->input('query') . '%')
                        ->orWhere('text', 'LIKE', '%' . $request->input('query') . '%');
            })
            ->orderBy('published_at', 'DESC')->paginate(12);

        return view('welcome', ['posts' => $posts, 'query' => $request->input('query')]);
    }

    /**
     * Display posts with the specified tag.
     *
     * @param  int  $id
     * @return View
     */
    public function tag($id)
    {
        $posts = Post::where('status', Post::PUBLISHED)->whereHas('tags', function ($query) use ($id) {
            return $query->where('tag_id', '=', $id);
        })->orderBy('published_at', 'DESC')->paginate(12);

        $tag = Tag::find($id);

        return view('welcome', ['posts' => $posts, 'tag' => $tag]);
    }

    /**
     * Show the form for creating a new post.
     *
     * @return View
     */
    public function create()
    {
        return view('posts.create', [
            'categories' => Category::all(),
            'tags' => Tag::all()
        ]);
    }

    /**
     * Store a newly created post in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Post  $post
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request, Post $post)
    {
        $post->title = $request->input('title');
        $post->text = $request->input('text');
        $post->user_id = $request->user()->id;
        $post->status = Post::UNPUBLISHED;
        $post->save();

        $post->category()->save(new PostCategory(['category_id' => $request->input('category')]));

        $image_name = time().'.'.$request->image->extension();
        $request->image->move(public_path('images/posts'), $image_name);

        $post->image()->save(new PostImage(['image' => $image_name]));

        foreach ($request->input('tags') as $tag)
        {
            $postTag = new PostTag(['tag_id' => $tag]);

            $post->tags()->save($postTag);
        }

        return redirect()->route('post.edit', $post)->withStatus(__('Post successfully created.'));
    }

    /**
     * Display the specified post.
     *
     * @param  int  $id
     * @return View
     */
    public function show($id)
    {
        return view('post', ['post' => Post::find($id)]);
    }

    /**
     * Show the form for editing the specified post.
     *
     * @param  Post  $post
     * @return View
     */
    public function edit(Post $post)
    {
        if (! Gate::allows('edit-post', $post)) {
            abort(403);
        }

        $categories = Category::all();
        $tags = Tag::all();

        return view('posts.edit', compact('post', 'categories', 'tags'));
    }

    /**
     * Update the specified post in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, Post $post)
    {

        if (! Gate::allows('update-post', $post)) {
            abort(403);
        }

        $post->title = $request->input('title');
        $post->text = $request->input('text');

        if ($request->input('publish')) {
            if (Gate::allows('publish-post', $post)) {
                $post->status = Post::PUBLISHED;
            } else {
                $post->status = Post::ON_APPROVAL;
                Mail::to(User::where('role', 'admin')->first()->email)->send(new \App\Mail\PublishPostNotification($post));
            }
        } else {
            if($post->status == Post::ON_APPROVAL || $post->status == Post::ON_APPROVAL) {
                $post->status = Post::ON_APPROVAL;
            } else {
                $post->status = Post::UNPUBLISHED;
            }
            $post->published_at = null;
        }

        $post->save();

        if ($request->image)
        {
            $image_name = time().'.'.$request->image->extension();
            $request->image->move(public_path('images/posts'), $image_name);

            $post->image()->update(['image' => $image_name]);
        }

        $post->category()->update(['category_id' => $request->input('category')]);

        $post->tags()->delete();

        foreach ($request->input('tags') as $tag)
        {
            $post->tags()->create(['tag_id' => $tag]);
        }

        return redirect()->route('post.edit', $post)->withStatus(__('Post successfully updated.'));
    }

    /**
     * Remove the specified post from storage.
     *
     * @param  int  $id
     * @return View
     */
    public function destroy(Post $post)
    {
        if (! Gate::allows('delete-post', $post)) {
            abort(403);
        }

        $post->delete();

        return view('posts.index', ['posts' => Post::paginate(15)]);
    }
}
