<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'required', 'min:3', 'max:255'
            ],
            'text' => [
                'required', 'min:10'
            ],
            'category' => [
                'required'
            ],
            'tags' => [
                'required', 'array', 'min:2'
            ],
            'image' => [
                'file', 'max:5000'
            ]
        ];
    }
}
