<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public const PUBLISHED = 2;
    public const ON_APPROVAL = 1;
    public const UNPUBLISHED = 0;

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function category()
    {
        return $this->hasOne(PostCategory::class, 'post_id', 'id');
    }

    public function image()
    {
        return $this->hasOne(PostImage::class, 'post_id', 'id');
    }

    public function tags()
    {
        return $this->hasMany(PostTag::class, 'post_id', 'id');
    }
}
