<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = FALSE;

    public function post_categories()
    {
        return $this->hasMany(PostCategory::class, 'category_id', 'id');
    }
}
