<?php

namespace App\Providers;

use App\Post;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('create-user', function (User $user) {
            return $user->role == 'admin';
        });

        Gate::define('edit-user', function (User $user, User $profile) {
            return $user->role == 'admin' || $user->id === $profile->id;
        });

        Gate::define('update-user', function (User $user, User $profile) {
            return $user->role == 'admin' || $user->id === $profile->id;
        });

        Gate::define('delete-user', function (User $user, User $profile) {
            return $user->role == 'admin' || $user->id === $profile->id;
        });

        Gate::define('edit-post', function (User $user, Post $post) {
            return $user->role == 'admin' || $user->id === $post->user_id;
        });

        Gate::define('update-post', function (User $user, Post $post) {
            return $user->role == 'admin' || $user->id === $post->user_id;
        });

        Gate::define('publish-post', function (User $user, Post $post) {
            return $user->role == 'admin';
        });

        Gate::define('delete-post', function (User $user, Post $post) {
            return $user->role == 'admin' || $user->id === $post->user_id;
        });
    }
}
