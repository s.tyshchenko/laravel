<?php

use Illuminate\Database\Seeder;
use Intervention\Image\Facades\Image;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class)
            ->create()
            ->each(function ($user) {
                $user->avatar()->save(new \App\UserAvatar(['image' => 'default.jpg']));
            });
    }
}
