<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 0;

        do
        {
            $post_id = \App\Post::inRandomOrder()->first()->id;
            $tag_id = \App\Tag::inRandomOrder()->first()->id;

            if (!\App\PostTag::where('post_id', $post_id)->where('tag_id', $tag_id)->get()->count())
            {
                DB::table('post_tags')->insert([
                    'post_id' => $post_id,
                    'tag_id' => $tag_id
                ]);

                $i++;
            }
        }
        while($i < 100);
    }
}
