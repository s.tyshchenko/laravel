<?php

use App\Post;
use App\PostCategory;
use App\PostImage;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Post::class, 36)
            ->create()
            ->each(function ($post) {
                $post->category()->save(factory(PostCategory::class)->make());
                $post->image()->save(new PostImage(['image' => 'default.jpg']));
            });
    }
}
