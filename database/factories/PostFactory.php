<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'user_id' => function(){
            return \App\User::inRandomOrder()->first()->id;
        },
        'title' => substr($faker->sentence(5, true), 0, -1),
        'text' => $faker->text(1000),
        'status' => Post::PUBLISHED,
        'published_at' => $faker->time('Y-m-d H:i:s')
    ];
});
