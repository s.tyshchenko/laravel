<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    $category_name = substr($faker->sentence(2, true), 0, -1);

    return [
        'name' => $category_name,
        'slug' => strtolower(str_replace(' ', '-', $category_name))
    ];
});
