<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Tag;
use Faker\Generator as Faker;

$factory->define(Tag::class, function (Faker $faker) {
    $tag_name = substr($faker->sentence(2, true), 0, -1);

    return [
        'name' => $tag_name,
        'slug' => strtolower(str_replace(' ', '-', $tag_name))
    ];
});
