<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'welcome', 'uses' => 'PostController@search']);
Route::get('search', ['as' => 'welcome', 'uses' => 'PostController@search']);
Route::post('search', ['as' => 'post.search', 'uses' => 'PostController@search']);
Route::get('search/tag/{id}', ['as' => 'post.tag', 'uses' => 'PostController@tag']);
Route::get('post/{id}', ['as' => 'post.show', 'uses' => 'PostController@show'])->where(['id' => '[0-9]+']);;

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('user', 'UserController', ['except' => ['show']]);
    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
    Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

Route::group(['middleware' => 'auth'], function () {
    Route::resource('post', 'PostController');
});
