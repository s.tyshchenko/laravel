@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
    <div class="header bg-gradient-primary py-7 py-lg-8">
        @if(isset($tag))
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h3 class="font-weight-300 text-white">{{ __('Posts by tag') }} <span class="badge badge-primary text-white" style="font-size: 14px;"> {{ $tag->name }}</span></h3>
                    </div>
                </div>
            </div>
        @endif
        <div class="container">
            <div class="row">
                @forelse ($posts as $post)
                <div class="col-6 col-md-3 p-3 d-flex">
                    <div class="card">
                        <img class="card-img-top" height="150" src="{{ asset('images/posts') }}/{{ $post->image->image }}" alt="Post image">
                        <div class="card-body d-flex flex-column align-items-start">
                            <h5 class="card-title" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;width: 100%;">{{ $post->title }}</h5>
                            <p class="card-text">{{ substr($post->text, 0, 49) }}...</p>
                        </div>
                        <div class="my-2 mx-4" style="max-height: 105px; overflow: scroll;">
                            @foreach($post->tags as $tag)
                                <a href="{{ route('post.tag', $tag->tag) }}" class="badge badge-primary">{{ $tag->tag->name }}</a>
                            @endforeach
                        </div>
                        <p class="mb-0 mt-1 mx-4 text-sm">
                            <span class="text-success mr-2">{{ $post->user->name }}</span>
                            <span class="text-nowrap">@formatDate($post->published_at)</span>
                        </p>
                        <a href="{{ route('post.show', $post) }}" class="btn btn-primary align-self-start m-4">View</a>
                    </div>
                </div>
                @empty
                    <div class="col-12">
                        <h1 class="text-white">{{ __('No posts found for') }}: <span class="font-weight-300 font-italic">{{ $query }}</span></h1>
                    </div>
                @endforelse

            </div>
        </div>
        <div class="separator separator-bottom separator-skew zindex-100">
            <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
            </svg>
        </div>
    </div>

    <div class="container mt--10 pb-5">
        <nav class="d-flex justify-content-end" aria-label="...">
            {{ $posts->links() }}
        </nav>
    </div>
@endsection
