@extends('layouts.app', ['title' => __('Post Management')])

@section('content')
    @include('users.partials.header', ['title' => __('Add Post')])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Post Management') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('post.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('post.store') }}" autocomplete="off" enctype="multipart/form-data">
                            @csrf

                            <h6 class="heading-small text-muted mb-4">{{ __('Post information') }}</h6>
                            <div class="pl-lg-4">
                                <div class="col-12 pr-0 pl-0 float-left">
                                    <div class="form-group{{ $errors->has('title') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-title">{{ __('Title') }}</label>
                                        <input type="text" name="title" id="input-title" class="form-control form-control-alternative{{ $errors->has('title') ? ' is-invalid' : '' }}" placeholder="{{ __('Title') }}" value="{{ old('title') }}" required autofocus>

                                        @if ($errors->has('title'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6 pr-0 pl-0 pr-md-4 pl-md-0 float-left">
                                    <div class=" form-group{{ $errors->has('category') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="select-category" >{{ __('Category') }}</label>
                                        <select name="category" id="select-category" class="form-control{{ $errors->has('category') ? ' is-invalid' : '' }}" required>
                                            @foreach($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 pr-0 pl-0 pl-md-4 pr-md-0 float-left">
                                    <div class=" form-group{{ $errors->has('tags') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="select-tags" >{{ __('Tags') }}</label>
                                        <select multiple name="tags[]" id="select-tags" class="form-control{{ $errors->has('tags[]') ? ' is-invalid' : '' }}" required>
                                            @foreach($tags as $tag)
                                                <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('tags[]'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('tags[]') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-12 pr-0 pl-0 float-left">
                                    <div class="form-group{{ $errors->has('text') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-text">{{ __('Text') }}</label>
                                        <textarea name="text" id="input-text" class="form-control form-control-alternative{{ $errors->has('text') ? ' is-invalid' : '' }}" placeholder="{{ __('Text') }}" required style="min-height: 300px;">{{ old('text') }}</textarea>
                                        @if ($errors->has('text'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('text') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group{{ $errors->has('image') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-image">{{ __('Image') }}</label>
                                        <input type="file" accept="image/png, image/jpeg, image/jpg" name="image" id="input-image" class="form-control form-control-alternative{{ $errors->has('image') ? ' is-invalid' : '' }}" required/>
                                        @if ($errors->has('image'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
