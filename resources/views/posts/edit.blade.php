@extends('layouts.app', ['title' => __('Post Management')])

@section('content')
    @include('users.partials.header', ['title' => __('Edit Post')])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">
                                    {{ __('Post status') }}:
                                    @if($post->status == 0)
                                        <span class="badge badge-primary" style="font-size: 14px;">{{ __('Unpublished') }}</span>
                                    @elseif($post->status == 1)
                                        <span class="badge badge-info" style="font-size: 14px;">{{ __('On approval') }}</span>
                                    @elseif($post->status == 2)
                                        <span class="badge badge-success" style="font-size: 14px;">{{ __('Published') }}</span>
                                    @endif
                                </h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('post.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('post.update', $post) }}" autocomplete="off" enctype="multipart/form-data">
                            @csrf
                            @method('put')

                            <h6 class="heading-small text-muted mb-4">{{ __('Post information') }}</h6>
                            <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-title">{{ __('Title') }}</label>
                                    <input type="text" name="title" id="input-title" class="form-control form-control-alternative{{ $errors->has('title') ? ' is-invalid' : '' }}" placeholder="{{ __('Title') }}" value="{{ old('title', $post->title) }}" required autofocus>

                                    @if ($errors->has('title'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="col-md-6 pr-0 pl-0 pr-md-4 pl-md-0 float-left ">
                                    <div class=" form-group{{ $errors->has('category') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="select-category" >{{ __('Category') }}</label>
                                        <select multiple name="category" id="select-category" class="form-control{{ $errors->has('category') ? ' is-invalid' : '' }}" required>
                                            @foreach($categories as $category)
                                                @if($category->id == $post->category->category_id)
                                                    <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                                                @else
                                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 pr-0 pl-0 pl-md-4 pr-md-0 float-left">
                                    <div class=" form-group{{ $errors->has('tags') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="select-tags" >{{ __('Tags') }}</label>
                                        <select multiple name="tags[]" id="select-tags" class="form-control{{ $errors->has('tags') ? ' is-invalid' : '' }}" required>
                                            @foreach($tags as $tag)
                                                @if(in_array($tag->id, array_column($post->tags->toArray(), 'tag_id')))
                                                    <option value="{{ $tag->id }}" selected>{{ $tag->name }}</option>
                                                @else
                                                    <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('text') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-text">{{ __('Text') }}</label>
                                    <textarea name="text" id="input-text" class="form-control form-control-alternative{{ $errors->has('text') ? ' is-invalid' : '' }}" placeholder="{{ __('Text') }}" required style="min-height: 300px;">{{ old('text', $post->text) }}</textarea>
                                    @if ($errors->has('text'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('text') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group float-left col-12 d-flex flex-wrap flex-direction-row align-items-center{{ $errors->has('image') ? ' has-danger' : '' }}">
                                    <label class="form-control-label col-12" for="input-image">{{ __('Image') }}</label>
                                    <input type="file" accept="image/png, image/jpeg" name="image" id="input-image" class="form-control col-md-3 col-12 float-left form-control-alternative{{ $errors->has('image') ? ' is-invalid' : '' }}"/>
                                    @if ($errors->has('image'))
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                    @endif
                                    @if($post->image)
                                        <div class="col-md-9 col-12 float-left d-flex justify-content-center">
                                            <img style="max-width: 90%; border-radius: .375rem;" src="{{ asset('images/posts') }}/{{ $post->image->image }}">
                                        </div>
                                    @endif
                                </div>

                                <div class="text-center col-12 float-left">
                                    <button type="submit" class="btn btn-primary mt-4">{{ __('Save') }}</button>
                                    @if(Gate::allows('publish-post', $post))
                                        @if($post->status == 0 || $post->status == 1)
                                            <input type="submit" name="publish" class="btn btn-success mt-4" value="{{ __('Publish') }}">
                                        @else
                                            <input type="submit" name="unpublish" class="btn btn-secondary mt-4" value="{{ __('Unpublish') }}">
                                        @endif
                                    @else
                                        @if($post->status == 1 || $post->status == 2)
                                            <input type="submit" name="unpublish" class="btn btn-secondary mt-4" value="{{ __('Unpublish') }}">
                                        @else
                                            <input type="submit" name="publish" class="btn btn-success mt-4" value="{{ __('Publish') }}">
                                        @endif
                                    @endif
                                    <button type="button" class="btn btn-danger mt-4" onclick="confirm('{{ __("Are you sure you want to delete this post?") }}') ? document.getElementById('delete-post-form').submit() : ''">{{ __('Delete') }}</button>
                                </div>
                            </div>
                        </form>
                        <form action="{{ route('post.destroy', $post) }}" id="delete-post-form" method="post">
                            @csrf
                            @method('delete')
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
