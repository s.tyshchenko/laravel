@extends('layouts.app')

@section('content')
    @include('layouts.headers.empty')

    <div class="container-fluid mt--9">
        <div class="row">
            <div class="col-xl-8 mb-5 mb-xl-0">
                <div class="card bg-gradient-default shadow">
                    <div class="card-header bg-transparent">
                        <div class="row align-items-center">
                            <div class="col">
                                <h6 class="text-uppercase text-light ls-1 mb-1">{{ $post->category->category->name }}</h6>
                                <h2 class="text-white mb-0">{{ $post->title }}</h2>
                            </div>
                        </div>
                    </div>
                    <div class="card-body py-0 px-0">
                        <!-- Image -->
                        <img width="100%" src="{{ asset('images/posts') }}/{{ $post->image->image }}">
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="card shadow">
                    <div class="card-body bg-transparent">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h6 class="text-uppercase text-muted ls-1 mb-1">{{ __('Author') }}</h6>
                                <h2 class="mb-0">{{ $post->user->name }}</h2>
                            </div>
                            <div class="col-4">
                                <img height="50px" style="border-radius: 50%;" alt="Publisher avatar" src="{{ asset('images/users') }}/{{ $post->user->avatar->image }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card shadow mt-4">
                    <div class="card-body bg-transparent">
                        <div class="row align-items-center">
                            <div class="col-12">
                                <h6 class="text-uppercase text-muted ls-1 mb-1">{{ __('Publication date') }}</h6>
                                <h2 class="mb-0">@formatDate($post->published_at)</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card shadow mt-4">
                    <div class="card-body bg-transparent">
                        <div class="row align-items-center">
                            <div class="col-12">
                                <h6 class="text-uppercase text-muted ls-1 mb-1">{{ __('Tags') }}</h6>
                                <h2 class="mb-0">
                                    @foreach($post->tags as $tag)
                                        <a href="{{ route('post.tag', $tag->tag) }}" class="badge badge-primary">{{ $tag->tag->name }}</a>
                                    @endforeach
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-xl-8 mb-5 mb-xl-0">
                <div class="card shadow">
                    <div class="card-body bg-transparent">
                    {{ $post->text }}
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
